import * as functions from 'firebase-functions';
import {environment} from "../../src/environments/firebase";
const admin = require('firebase-admin');
admin.initializeApp(environment.firebaseConfig);

export const writeDevice = functions.https.onRequest(async  (req, res) => {
    const body = req.body;

    functions.logger.info("Here is req body:");
    functions.logger.info(req.body);
    const device = {
        currentState: body.data,
        lastCheckin: new Date().toISOString()

    };
    await admin.firestore().doc('device/1').update(device);
    admin.firestore().doc('device/1').get().then(function(docDevice: any){
        functions.logger.info(docDevice.data());

        return res.json({
            targetState: docDevice.data().targetState
        });

    });

});

export const getTrailStatus = functions.https.onRequest(async  (req, res) => {
    admin.firestore().doc('device/1').get().then(function(docDevice: any){
        return res.json({
            targetState: docDevice.data().targetState
        });

    });

});
