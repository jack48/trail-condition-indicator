import { Injectable } from '@angular/core';
import {AngularFirestore} from "@angular/fire/firestore";
import {Observable} from "rxjs";
import {Device} from "./device";



@Injectable({
  providedIn: 'root'
})
export class DatabaseService {

  device: Observable<Device>;

  constructor(private firestore: AngularFirestore) {
    this.device = firestore.doc('/device/1').valueChanges();
  }

  public saveDevice(dev: Device){
    return this.firestore.doc('/device/1').update(dev);
  }
}
