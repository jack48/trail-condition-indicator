export interface Device {
    currentState?: string;
    targetState?: string;
    lastUpdate?: string;  // RED RED-GREEN
    batteryLevel?: string;
    lastCheckin?: string;
    sleepIntervalSeconds?: number;

}
