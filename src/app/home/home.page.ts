import { Component } from '@angular/core';
import {DatabaseService} from "../database.service";
import {Observable, Subscription} from "rxjs";
import {Device} from "../device";

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  device: Device;
  deviceSub: Subscription;

  constructor(private databaseService: DatabaseService) {
    this.deviceSub = databaseService.device.subscribe(device => {
      this.device = device;
    });
  }





}
