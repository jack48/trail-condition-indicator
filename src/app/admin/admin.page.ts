import { Component, OnInit } from '@angular/core';
import {Device} from "../device";
import {Subscription} from "rxjs";
import {DatabaseService} from "../database.service";

@Component({
  selector: 'app-admin',
  templateUrl: './admin.page.html',
  styleUrls: ['./admin.page.scss'],
})
export class AdminPage  {

  device: Device;
  deviceSub: Subscription;

  constructor(private databaseService: DatabaseService) {
    console.log("In constructor")
    console.dir(databaseService);
    this.deviceSub = databaseService.device.subscribe(device => {
      console.log("Device updated from db...", device);
      this.device = device;
    });
  }

  public changeStatus(targetStatus: string){
    console.log("New status selected.");
    this.device.targetState = targetStatus;
    this.databaseService.saveDevice(this.device).then(res => {
      console.log("New values saved to DB");
    });

  }
}
